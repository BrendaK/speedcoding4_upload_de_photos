<?php
if (isset($_POST['submit']))
{
    $maxSize = 50000;
    $validExt = array('.jpg', '.jpeg', '.gif', '.png');

    if ($_FILES['uploaded_file']['error'] > 1)
    {
        echo "erreur de transfert";
        die;
    }
    $fileSize = $_FILES['upload_file']['size'];

    if ($fileSize > $maxSize)
    {
        echo"Le fichier est trop gros";
        die;
    }
    
    $fileName = $_FILES['upload_file']['name'];
    $fileExt = "." . strtolower(substr(strrchr($fileName, '.'), 1));

    if(!in_array($fileExt, $validExt))
    {
        echo "le fichier n'est pas une image";
        die;
    }

    $tmpName = $_FILES['upload_file']['tmp_name'];
    $uniqueName = md5(uniqid(rand(), true));
    $fileName = "upload/" . $uniqueName . $fileExt;
    $result = move_uploaded_file($tmpName, $fileName);

    if($result)
    {
        echo"transfert terminé";
    }
}
?>