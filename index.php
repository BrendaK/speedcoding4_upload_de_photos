<?php include "upload.php" ?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <title>Upload de photos</title>
</head>
<body>
    <form action="upload.php" method="POST" enctype="multipart/form-data">
        <h2>Upload des photos</h2>
        <label for="upload_file">Image:</label>
        <input type="file" name="upload_file"/>
        <input type="submit" name="submit" value="Upload"/>
        <p><strong>Note:</strong> Seuls les formats .jpg, .jpeg, .gif, .png sont autorisés jusqu'à une taille maximale de 5 Mo.</p>
    </form>
</body>
</html>

